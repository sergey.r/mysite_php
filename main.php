<html>
<head>
    <meta charset="UTF-8">
    <title>My Site</title>
    <link rel="stylesheet" href="static/style.css">
</head>
<body>
<div class="flex-container">
    <div class="header">
        <?php
        include "include/main_menu.inc.php";
        ?>
    </div>
    <div>
        <img src="img/of.jpg">
        <div class="about">
            <?php echo $greeting ?>
            <?php echo $about ?>
        </div>
        <div class="knowledge">
            <?php include "include/knowledge.inc.php" ?>
        </div>
    </div>
</body>
</html>